package com.netty.utils;


public class Constants {


    //文件collection
    public static final String FILE_COLLECTION="FILE_INFO";

    //上传任务collection
    public static final String UPLOAD_TASK_COLLECTION="UPLOAD_TASK";

    //下载任务collection
    public static final String DOWNLOAD_TASK_COLLECTION="DOWNLOAD_TASK";

    //websocket服务器ip
    public static final String WEBSOCKET_IP="0.0.0.0";

    //websocket服务器端口
    public static final int WEBSOCKET_PORT=8787;


    //http服务器端口
    public static final int HTTP_PORT=8883;


    //服务器接收或下载的临时文件存储地
    public static final String BASE_DIRECTORY="/opt/soft/nettyserver/temp/";

    //最终上传成功的文件存储地
    public static final String FILE_DIRECTORY="/opt/soft/nettyserver/file/";

    //mongodb host
    public static final String MONGODB_HOST="192.168.4.91";

    //mongodb port
    public static final Integer MONGODB_PORT=27017;

    //mongodb user
    public static final String MONGODB_USER="fileex";

    //mongodb user
    public static final String MONGODB_PWD="fileex";

    //mongodb user
    public static final String MONGODB_DATABASE="fileex";


    //每执行多少次的数据包就推送进度和速度
    public static final Integer PUSH_COUNT=50;




}
